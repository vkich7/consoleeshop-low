﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IShopImitation
{
    public enum FoundResult
    {
        NotFound = -1
    }
    public class GuestUser: IUser, IEquatable<GuestUser>
    {
        string Name { get; set; }
        string Password;
        public GuestUser()
        {

        }
        public GuestUser(string name, string password)
        {
            this.Name = name;
            this.Password = password;
        }
        public bool Equals(GuestUser other)
        {
            if (this.Name == other.Name && this.Password == other.Password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        IUser IUser.Register(string name, string password)
        {
            this.Name = name;
            this.Password = password;
            return this;
        }

        IEnumerable<GoodThing> IUser.SeeGoods(List<GoodThing> GList)
        {
            foreach (GoodThing g in GList)
                yield return g;
        }

        IEnumerable<GoodThing> IUser.FindGoods(List<GoodThing> GList, string NameContainsSymbols)
        {
            foreach (GoodThing g in GList)
                if (g.Name.Contains(NameContainsSymbols))
                    yield return g;
        }
    }
        public class GoodThing
        {
        public int ID { get; private set; }
        public string Name { get; private set; }
        public int Category { get; }
        public string Description { get; }
        public decimal Price { get; private set; }
        public GoodThing(int id, string name, int category, decimal price)
        {
            this.Description = "";
            this.ID = id;
            this.Name = name;
            this.Category = category;
            this.Price = price;
        }
    }
    interface IUser
    {
        public IUser Register(string name, string password);
        IEnumerable<GoodThing> SeeGoods(List<GoodThing> GList);
        IEnumerable<GoodThing> FindGoods(List<GoodThing> GList, string NameContainsSymbols);
    }
    class GoodOrders
    {
        public IUser User { get; private set; }
        IList<GoodThing> Ordered;
        public GoodOrders(IUser user, GoodThing good_thing)
        {
            Ordered?.Add(good_thing);
            User = user;
        }
    }

    class RegisteredUsersList
    {
        List<GuestUser> Users;
        public RegisteredUsersList()
        {
            Users = new List<GuestUser>();
        }
        public bool AddUser(GuestUser user)
        {
            if (Users.Contains(user))
                return false;
            else
            {
                Users.Add(user);
                return true;
            }       
        }
        public bool IsRegistered(GuestUser user)
        {
            if (Users.Contains(user))
                return true;
            else return false;
        }
    }

    class GoodsList
    {
        public List<GoodThing> Goods;
        public GoodsList()
        {
            Goods = new List<GoodThing>
            {
                new GoodThing(1, "Something1", 1, 100),
                new GoodThing(1, "Something2", 2, (decimal)19.55)
            };
        }
        public void SeeGoods(IEnumerable<GoodThing> gl)
        {
            foreach (GoodThing g in gl)
                Console.Write($" name: {g.Name} price: {g.Price}");
        }

        public IEnumerable<GoodThing> FindGood(string NameContainsSymbols, GoodsList gl)
        {
            foreach (GoodThing g in gl.Goods)
                if (g.Name.Contains(NameContainsSymbols))
                    yield return g;
        }
    }

    class Program
    {
        static RegisteredUsersList ru = new RegisteredUsersList();
        public static void ShowMenu(string[] menu, ref int sel)
        {
            Console.Clear();
            int n = menu.Length;
            for (int i = 0; i < n; i++)
            {
                if (i == sel)
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine(menu[i]);
                Console.ResetColor();
            }
            ConsoleKeyInfo info = Console.ReadKey(true);
            switch (info.Key)
            {
                case ConsoleKey.UpArrow:
                    sel--;
                    break;
                case ConsoleKey.DownArrow:
                    sel++;
                    break;
                case ConsoleKey.Enter:
                    Console.WriteLine( "{0} has been choosen", menu[sel]);
                    
                    GuestUser nu = new GuestUser();
                    GoodsList gl = new GoodsList();

                    switch (sel)
                    {
                        case 5:
                            Environment.Exit(0);
                            break;
                        case 0:
                            Console.WriteLine("Good(s) is(are): ");
                            gl.SeeGoods(gl.Goods);
                            break;
                        case 1:
                            Console.WriteLine("Type the symbols the name contains: ");
                            string NameForFinding = Console.ReadLine();
                            if ((gl.FindGood(NameForFinding, gl)).Any())
                                gl.SeeGoods(gl.FindGood(NameForFinding, gl));
                            else Console.WriteLine("Not found.");
                            break;
                        case 2:
                            Console.Write("Input user name:");
                            string NewUserName = Console.ReadLine();
                            string NewUserPassword1, RepeatPassword;
                            Console.Write("Input password:");
                            NewUserPassword1 = Console.ReadLine();
                            Console.Write("Retype password:");
                            RepeatPassword = Console.ReadLine();
                            if (RepeatPassword == NewUserPassword1)
                                (nu as IUser).Register(NewUserName, NewUserPassword1);
                            else
                            {
                                Console.WriteLine("Password doesn't match.");
                                break;
                            }
                            if (ru.AddUser(nu))
                                Console.WriteLine("Successfully registered.");
                            else Console.WriteLine("Not registered.");
                            break;
                        case 3:
                            Console.Write("Input user name:");
                            string UserName = Console.ReadLine();
                            Console.Write("Input password:");
                            string UserPassword = Console.ReadLine();
                            GuestUser check_user = new  GuestUser(UserName, UserPassword );
                            if (ru.IsRegistered(check_user))
                                Console.WriteLine($"{UserName} is logged in.");
                            else
                                Console.WriteLine($"{UserName} is not registered or password is incorrect");
                            break;
                    }
                    Console.ReadKey();
                    break;
            }
            if (sel < 0)
                sel = n - 1;
            else if (sel >= n)
                sel = 0;
        }
        static void Main(string[] args)
        {
            {
                string[] menu = { "Show good's list", "Find goods", "Register",
                    "Login", "It's getting hard", "Exit" };
                int sel = 1;
                bool cont = true;
                while (cont)
                {
                    ShowMenu(menu, ref sel);
                }
            }
        }
    }
}
